import path from 'path';
import { spawn  } from 'child_process';
import { promises as fs } from 'fs';
import { promisify } from 'util';

import cheerio from 'cheerio';
import decompress from 'decompress';
import fetch from 'node-fetch';

const addonsFolderPath = './test';
const metadataFilePath = path.resolve(addonsFolderPath, '.wow-classic-addon-manager.json');

async function dmenu(items, prompt = '') {
  const process = spawn('dmenu', ['-l', '20', '-p', prompt]);
  const { stdin, stdout, stderr } = process;
  stdin.end(items.join('\n'));
  const result = await new Promise((resolve, reject) => {
    stdout.on('data', (data) => resolve(data.toString().trim()));
    process.on('close', (code) => {
      if (code !== 0) {
        reject(`Exited with status code: ${code}`);
      }
    });
  });
  if (!items.includes(result)) {
    return dmenu(items, prompt);
  }
  return result;
}

async function fetchUrl(url) {
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(response.statuText);
  }
  return response.text();
}

async function fetchListPage(pageNumber) {
  // return fetchUrl(`https://www.wowinterface.com/downloads/index.php?cid=160&sb=dec_date&so=desc&pt=f&page=${pageNumber}`);
  return fetchUrl(`http://localhost:8080/downloads/index.php?cid=160&sb=dec_date&so=desc&pt=f&page=${pageNumber}`);
}

async function readMetadataFile() {
  try {
    return JSON.parse(await fs.readFile(metadataFilePath));
  } catch(error) {
    console.warn('Failed to read metadata file, continue with empty meta...', error);
    return { installed: {} };
  }
}

async function writeMetadataFile(metadata) {
  await fs.writeFile(metadataFilePath, JSON.stringify(metadata, null, 2));
}

async function withUpToDateDetails(addon) {
  const $addonPage = cheerio.load(await fetchUrl(addon.url));
  const version = $addonPage('#version').text().split(' ')[1];
  const downloadPageUrl = $addonPage('#download a').attr('href');
  return {
    ...addon,
    version,
    downloadPageUrl,
  };
}

async function getDownloadUrl(downloadPageUrl) {
  const $downloadPage = cheerio.load(await fetchUrl(`http://www.wowinterface.com${downloadPageUrl}`));
  return $downloadPage('.manuallink a').attr('href');
}

async function installAddon(addon) {
  const downloadUrl = await getDownloadUrl(addon.downloadPageUrl);
  const fileResponse = await fetch(downloadUrl);
  if (!fileResponse.ok) {
    throw new Error(`Failed to download addon: ${fileResponse.statusText}`);
  }
  const buffer = await fileResponse.buffer();
  await decompress(buffer, addonsFolderPath);
  console.log(`${addon.name} (${addon.version}) installed.`);
  return addon;
}

async function dmenuInstallAddon() {
  const addonsList = await fetchAddonsList();
  const selectedAddonName = await dmenu(addonsList.map(({ name }) => name), 'Install addon:');
  const selectedAddon = addonsList.find(({ name }) => name == selectedAddonName);
  const addonWithDetails = await withUpToDateDetails(selectedAddon);
  const downloadUrl = getDownloadUrl(addonWithDetails.downloadPageUrl);
  return await installAddon(addonWithDetails);
}

async function updateAllAddons(installedAddons) {
  const processedAddons = await Promise.all(Object.values(installedAddons).map(async (addon) => {
    const upToDateAddon = await withUpToDateDetails(addon);
    if (upToDateAddon.version !== addon.version) {
      await installAddon(upToDateAddon);
      console.log(`Updated ${addon.name} (${addon.version} -> ${upToDateAddon.version}).`);
      return upToDateAddon;
    }
    console.log(`${addon.name} (${addon.version}) is up to date.`);
    return null;
  }));
  return Object.assign({}, ...processedAddons.filter((addon) => addon).map((addon) => ({ [addon.id]: addon })));
}

async function fetchAddonsList() {
  const $firstPage = cheerio.load(await fetchListPage(1));
  const totalPages = Number($firstPage('.pagenav tr > td.vbmenu_control').text().split(' ').slice(-1)[0]);
  const pages = [
    $firstPage,
    ...(await Promise.all(
      new Array(totalPages - 1).fill().map(async (value, index) => {
        return cheerio.load(await fetchListPage(index + 2));
      })
    )),
  ];
  return pages.flatMap(($) => {
    return $('.file').toArray().map((fileDom) => {
      const $file = $(fileDom);
      const id = Number($file.attr('id').split('_')[1]);
      const $link = $file.find('h2 a');
      const name = $link.text();
      const url = `http://www.wowinterface.com/downloads/${$link.attr('href')}`;
      return {
        id,
        name,
        url,
      };
    });
  });
}

const operations = {
  'install addon': async (metadata) => {
    const installedAddon = await dmenuInstallAddon();
    return {
      ...metadata,
      installed: {
        ...metadata.installed,
        [installedAddon.id]: installedAddon,
      },
    };
  },
  'update all addons': async (metadata) => {
    const updatedAddons = await updateAllAddons(metadata.installed);
    return {
      ...metadata,
      installed: {
        ...metadata.installed,
        ...updatedAddons,
      },
    };
  },
};

async function main() {
  const metadata = await readMetadataFile();
  const operation = await dmenu(Object.keys(operations));
  const updatedMetadata = await operations[operation](metadata);
  await writeMetadataFile(updatedMetadata);
}

export default main;
